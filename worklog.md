[2021-05-12]

# Arduino IDE
* Download [Arduino IDE 1.8.13](https://www.arduino.cc/en/software)(Linux 64 bits) and install it.
* Error: `/dev/ttyACM0; Method name - openPort(); Exception type - Permission denied.`
    * Solution 1: Before uploading programs, we need to set the permission:
        ```
        thk arduino-1.8.13 $ sudo chmod a+rw /dev/ttyACM0 
        thk arduino-1.8.13 $ ll -h /dev/ttyACM0 
        crw-rw---- 1 root dialout 166, 0 May 13 00:02 /dev/ttyACM0
        thk arduino-1.8.13 $ sudo chmod a+rw /dev/ttyACM0 
        thk arduino-1.8.13 $ ll -h /dev/ttyACM0 
        crw-rw-rw- 1 root dialout 166, 0 May 13 00:02 /dev/ttyACM0
        ```
    * Solution 2 (better): Add user to the `dialout` group and re-log:
        ```
        sudo adduser thk dialout
        ```

[2021-05-15]
# TF Lite Micro

* Try the example of [yes/no](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro/examples/micro_speech#deploy-to-arduino).
    * The accuracy is terrible... (`yes` is easier to be recognized than `no`)


* __TODO__
    * TinyML Ch.5
    * https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/micro/examples/hello_world/hello_world_test.cc
    * Follow the [hello world](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro/examples/hello_world/train) example.

[2021-06-09]
* See [Micro Speech Training](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro/examples/micro_speech/train)
  for training new model.
* The training scripts are given in [Speech Commands Example](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/examples/speech_commands).

[2021-06-09]

# Teachable Machine
* Try [Teachable machine](https://teachablemachine.withgoogle.com/)
    * Train a model to detect `chi̍t`, `nn̄g`, and `saⁿ`.
    * The result is pretty good.
    * But the model cannot be converted to tflite for download.
    * The model cannot be used locally. 
      See `./tm_audio_test/index.html` for the uploaded url.

[2021-06-11]

## Arduino
* After uploading the `micro speech` example to the board, the message shows:
```
Sketch uses 173088 bytes (17%) of program storage space. Maximum is 983040 bytes.
Global variables use 74952 bytes (28%) of dynamic memory, leaving 187192 bytes for local variables. Maximum is 262144 bytes.
```

## Model conversion
* Create new env and install `tensorflowjs`:
    ```
    conda create -n tinyml_env
    conda activate tinyml_env
    conda install -c conda-forge python=3.6.8
    pip install tensorflowjs
    pip install PyInquirer==1.0.3
    ```
* Run wizard to get conversion command:
    ```
    tensorflowjs_wizard --dryrun
    tensorflowjs_converter --input_format=tfjs_layers_model --metadata= --output_format=keras_saved_model --quantize_uint8=* ./model.json ./converted_saved_model
    ```
* Run `tm_audio_1/converted_saved_model/pb2tflite.py` to get tflite model,
    then move it to `tm_audio_1/converted_tflite_micro/`.
* In `tm_audio_1/converted_tflite_micro/` run the following command to convert to CC file:
    ```
    xxd -i model.tflite > model_data.cc
    ```
* The model size seems too large...

### Change the model (CC file)

In `/home/thk/Arduino/libraries/Arduino_TensorFlowLite/examples/micro_speech/`.

* `cp micro_features_model.cpp micro_features_model.cpp.bak`
* Replace the content of `g_model[]` in line 35 and `g_model_len` at the end.
* Try to compile the model and get overflow error:
    ```
    /home/thk/Arduino/libraries/Arduino_TensorFlowLite/src/tensorflow/lite/micro/tools/make/downloads/kissfft/kiss_fft.c:378:9: note: in a call to built-in function 'memcpy'
    /home/thk/.arduino15/packages/arduino/tools/arm-none-eabi-gcc/7-2017q4/bin/../lib/gcc/arm-none-eabi/7.2.1/../../../../arm-none-eabi/bin/ld: micro_speech.ino.elf section `.text' will not fit in region `FLASH'
    /home/thk/.arduino15/packages/arduino/tools/arm-none-eabi-gcc/7-2017q4/bin/../lib/gcc/arm-none-eabi/7.2.1/../../../../arm-none-eabi/bin/ld: region `FLASH' overflowed by 4902896 bytes
    collect2: error: ld returned 1 exit status
    exit status 1
    Error compiling for board Arduino Nano 33 BLE.
    ```
* __So the model is too big__...


# References
* [Pete Warden talk about "tiny_conv" is too small to be useful...](https://github.com/adafruit/Adafruit_TFLite_Micro_Speech/issues/2)
* Follow this: [Simple audio recognition: Recognizing keywords](https://www.tensorflow.org/tutorials/audio/simple_audio)
* [tensorflow/tflite-micro](https://github.com/tensorflow/tflite-micro)

# Edge Impulse solution

* Original video given by Scott: https://youtu.be/vkX7_4jMD3A
* It links to: https://youtu.be/fRSVQ4Fkwjc

## Dataset preparation

Dataset is saved in:
```
/media/thk/transcend_2T/DataSet/audio/tgh_2021
```

### Audio files
* Use the UI on teachable machine to record audio files,
    which has nice GUI to give good visual reference for 1-second intervals.
* Download the recorded audio files.
* Write `audio_seg.sh` to seperate the audio files.

### Use data-curation scripts

* source: https://github.com/ShawnHymel/ei-keyword-spotting
* In `tinyml_env`, install the following libs:
    * `conda install -c conda-forge librosa` 
        and get the `No module named 'numba.decorators'` error.
        * solution: `conda install -c conda-forge numba=0.48`
    * `pip install soundfile`
* __NOTE__: 
    * This step takes long time to complete.
    * Need to include the `speech_commands_dataset` (or other audio files)
        to generate `unknown` class.
    * Example:
        ```
        python dataset-curation.py \
            -t "gaucha, liho" -n 1500 -w 1.0 -g 0.1 -s 1.0 -r 16000 -e PCM_16 \
            -b "/media/thk/transcend_2T/DataSet/audio/tgh_2021/_background_noise_" \
            -o "/media/thk/transcend_2T/DataSet/audio/tgh_2021/keywords_curated" \
            "/media/thk/transcend_2T/DataSet/audio/tgh_2021/speech_commands_dataset" \
            "/media/thk/transcend_2T/DataSet/audio/tgh_2021/custom_keywords"
        ```

### Result

* The train and test score in EI are too high to be true... @@
* The test on Arduino board is not bad. :-)
