python dataset-curation.py \
    -t "gaucha, liho" -n 1500 -w 1.0 -g 0.1 -s 1.0 -r 16000 -e PCM_16 \
    -b "/media/thk/transcend_2T/DataSet/audio/tgh_2021/_background_noise_" \
    -o "/media/thk/transcend_2T/DataSet/audio/tgh_2021/keywords_curated" \
    "/media/thk/transcend_2T/DataSet/audio/tgh_2021/speech_commands_dataset" \
    "/media/thk/transcend_2T/DataSet/audio/tgh_2021/custom_keywords"
